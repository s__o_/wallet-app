package com.example.simon.wallet.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.simon.wallet.models.account.BAccount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Simon on 18.11.18.
 */

@Dao
public interface BAccountDAO {
    @Query("SELECT * FROM BAccount")
    List<BAccount> getAll();

    @Insert
    void insertAll(BAccount ... accounts);

    @Delete
    void delete(BAccount account);
}
