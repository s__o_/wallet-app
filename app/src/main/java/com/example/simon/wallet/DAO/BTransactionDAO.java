package com.example.simon.wallet.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.simon.wallet.models.transactions.BTransaction;
import com.example.simon.wallet.models.transactions.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Simon on 18.11.18.
 */

@Dao
public interface BTransactionDAO {
    @Query("SELECT * FROM BTransaction ORDER BY date DESC")
    List<BTransaction> getAll();

    @Insert
    void insertAll(BTransaction ... transactions);

    @Delete
    void delete(BTransaction transaction);
}
