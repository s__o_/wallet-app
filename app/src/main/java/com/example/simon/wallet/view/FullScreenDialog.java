package com.example.simon.wallet.view;

import android.app.Activity;
import android.app.Dialog;
//import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.ColorInt;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.simon.wallet.MainActivity;

import com.example.simon.wallet.R;
import com.example.simon.wallet.models.transactions.BTransaction;
import com.example.simon.wallet.models.transactions.DefaultTransaction;
import com.example.simon.wallet.models.transactions.Transaction;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Simon on 27.09.18.
 */

public class FullScreenDialog extends DialogFragment
        implements DatePickerFragment.SetDateListener {

    private AddTransactionListener listener;
    private int type;

    @Override
    public void updateDate(String date) {
        TextView dateView = getView().findViewById(R.id.transaction_date);
        dateView.setText(date);
        Log.v("FullScreenDialog", date);
    }

    public interface AddTransactionListener {
        void addTransaction(Transaction trans);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (AddTransactionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnTimePickedListener.");
        }
    }

    public static FullScreenDialog newInstance(int style, int layout, int dialogType) {
        FullScreenDialog f = new FullScreenDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("style", style);
        args.putInt("layout", layout);
        args.putInt("type", dialogType);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(getArguments().getInt("style"), getArguments().getInt("style"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        int layout = getArguments().getInt("layout");
        View v = inflater.inflate(layout, container, false);

        TextView title = v.findViewById(R.id.transaction_title);
        TextView date = v.findViewById(R.id.transaction_date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        date.setText(dateFormat.format(new Date()).toString());

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.setTargetFragment(FullScreenDialog.this, 0);
                datePicker.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        this.type = getArguments().getInt("type");
        switch (this.type) {
            case MainActivity.EXPENSE:
                title.setText("Expense");
                break;
            case MainActivity.INCOME:
                title.setText("Income");
                break;
            case MainActivity.TRANSFER:
                title.setText("Transfer");
                break;
        }

        listener = (AddTransactionListener) getActivity();

        Button button = v.findViewById(R.id.save_transaction_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                BTransaction t = newTransaction(view.getRootView());
                Log.v("Dialog", "" + t.getValue());
                listener.addTransaction(t);
                dismiss();
            }
        });

        ImageView dismiss = v.findViewById(R.id.img_dialog_fullscreen_close);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams
                .SOFT_INPUT_STATE_VISIBLE);

        initSpinner(v, getArguments().getInt("type"));

        return v;
    }


    private void initSpinner(View v, int type) {
        Spinner spinnerSource = v.findViewById(R.id.transaction_spinner_source);
        Spinner spinnerTarget = v.findViewById(R.id.transaction_spinner_target);

        int arraySource = 0;
        int arrayTarget = 0;

        switch (type) {
            case MainActivity.EXPENSE:
                arraySource = R.array.accounts;
                arrayTarget = R.array.categories;
                break;
            case MainActivity.INCOME:
                arraySource = R.array.categories;
                arrayTarget = R.array.accounts;
                break;
            case MainActivity.TRANSFER:
                arraySource = R.array.accounts;
                arrayTarget = R.array.accounts;
                break;
        }


//        TypedValue tv = new TypedValue();
//        Resources.Theme t = v.getContext().getTheme();
//        t.resolveAttribute(R.attr.headerTextColor, tv, true);
//        Log.v("FullScreenDialog", tv.data+ "");

        ArrayAdapter<CharSequence> adapterSource = ArrayAdapter.createFromResource(
                this.getContext(), arraySource, R.layout.simple_spinner_item);

        ArrayAdapter<CharSequence> adapterTarget = ArrayAdapter.createFromResource(
                this.getContext(), arrayTarget, R.layout.simple_spinner_item);

//        TextView e1 = (TextView) spinnerSource.getChildAt(0);
//        int c = tv.data;
//        e1.setTextColor(getResources().getColor(c));

        adapterSource.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        adapterTarget.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);


//        spinnerSource.setDropDownViewResource(R.layout.spinner_item);
//        spinnerSource.setDropDownViewResource(R.layout.spinner_item);

        spinnerSource.setAdapter(adapterSource);
        spinnerTarget.setAdapter(adapterTarget);
    }

    private BTransaction newTransaction(View v) {
        EditText value = v.findViewById(R.id.transaction_value);
        TextView date = v.findViewById(R.id.transaction_date);
        EditText note = v.findViewById(R.id.transaction_note);

        Spinner spinnerSource = v.findViewById(R.id.transaction_spinner_source);
        Spinner spinnerTarget = v.findViewById(R.id.transaction_spinner_target);

        String category = null;
        int valueParsed = (int) (Double.parseDouble(value.getText().toString()) * 100);

        switch (this.type) {
            case MainActivity.EXPENSE:
                category = spinnerTarget.getSelectedItem().toString();
                valueParsed = valueParsed * (-1);
                break;
            case MainActivity.INCOME:
                category = spinnerSource.getSelectedItem().toString();
                break;
            case MainActivity.TRANSFER:
                category = "Transfer";
                break;
        }

        return new BTransaction(
                valueParsed,
                note.getText().toString(),
                category,
                date.getText().toString());
    }
}
