package com.example.simon.wallet.models.transactions;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Simon on 18.11.18.
 */
@Entity
public class BTransaction extends Transaction {

    @Ignore
    public BTransaction(int value, String desc, String category, String date) {
        super(value, desc, category, date);
    }

    public BTransaction(int id, int value, String desc, String category, String date){
        super(id, value, desc, category, date);
    }
}
