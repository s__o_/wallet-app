package com.example.simon.wallet.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.simon.wallet.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Simon on 22.09.18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private List<String> transactions;
    private int color = 0;
    private View parentView;

    private final int TYPE_NORMAL = 1;
    private final int TYPE_FOOTER = 2;
    private final int TYPE_HEADER = 3;
    private final String FOOTER = "footer";
    private final String HEADER = "header";

    public RecyclerViewAdapter(Context context) {
        this.context = context;
        transactions = new ArrayList();
    }

    public void setItems(List<String> data) {
        this.transactions.addAll(data);
        notifyDataSetChanged();
    }

    public void addItem(int position, String insertData) {
        transactions.add(position, insertData);
        notifyItemInserted(position);
    }

    public void addItems(List<String> data) {
        transactions.add(HEADER);
        transactions.addAll(data);
        notifyItemInserted(transactions.size() - 1);
    }

    public void setColor(int color) {
        this.color = color;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        parentView = parent;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_recycler_view, parent, false);
        return new RecyclerViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }


    @Override
    public int getItemViewType(int position) {
        String s = transactions.get(position);
        switch (s) {
            case HEADER:
                return TYPE_HEADER;
            case FOOTER:
                return TYPE_FOOTER;
            default:
                return TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }


    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(transactions, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }


    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private RelativeLayout rela_round;

        private RecyclerViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
    }
}
