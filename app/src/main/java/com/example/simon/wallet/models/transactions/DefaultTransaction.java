package com.example.simon.wallet.models.transactions;

import java.sql.Date;

/**
 * Created by Simon on 22.08.18.
 */

public class DefaultTransaction extends Transaction {

    public DefaultTransaction(int value, String desc, String category, String date) {
        super(value, desc, category, date);
    }
}
