package com.example.simon.wallet.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.simon.wallet.R;
import com.example.simon.wallet.models.transactions.BTransaction;
import com.example.simon.wallet.models.transactions.Transaction;

import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by Simon on 19.11.18.
 */

public class BTransactionRecyclerAdaper
        extends RecyclerView.Adapter<BTransactionRecyclerAdaper.ViewHolder> {

    private ArrayList dataset;

    @NonNull
    @Override
    public BTransactionRecyclerAdaper.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                    int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Transaction trans = (Transaction) this.dataset.get(position);

        TextView date = holder.textView.findViewById(R.id.list_item_date);
        date.setText(trans.getDate());

        TextView description = holder.textView.findViewById(R.id.list_item_label);

        if(trans.getDesc() == null | trans.getDesc().length() == 0){
            description.setVisibility(View.GONE);
            date.setGravity(Gravity.CENTER_VERTICAL);
        }else {
            description.setText(trans.getDesc());
            description.setVisibility(View.VISIBLE);
        }

        TextView category = holder.textView.findViewById(R.id.list_item_category);
        category.setText(trans.getCategory());

        TextView value = holder.textView.findViewById(R.id.list_item_value);
        value.setText(trans.getValueText());
    }

    @Override
    public int getItemCount() {
        return this.dataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View textView;
        public ViewHolder(View v){
            super(v);
            textView = v;
        }
    }

    public BTransactionRecyclerAdaper(ArrayList dataset) {this.dataset =
            dataset;}


}
