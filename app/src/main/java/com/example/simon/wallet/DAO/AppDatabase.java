package com.example.simon.wallet.DAO;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.simon.wallet.models.account.BAccount;
import com.example.simon.wallet.models.transactions.BTransaction;

/**
 * Created by Simon on 18.11.18.
 */

@Database(entities = {BTransaction.class, BAccount.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract BTransactionDAO bTransactionDAO();
    public abstract BAccountDAO bAccoutDAO();
}
