package com.example.simon.wallet.controller;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.simon.wallet.DAO.AppDatabase;
import com.example.simon.wallet.DAO.BAccountDAO;
import com.example.simon.wallet.DAO.BTransactionDAO;
import com.example.simon.wallet.adapter.BTransactionRecyclerAdaper;
import com.example.simon.wallet.adapter.RecyclerViewAdapter;
import com.example.simon.wallet.models.account.Account;
import com.example.simon.wallet.models.account.BAccount;
import com.example.simon.wallet.models.transactions.BTransaction;
import com.example.simon.wallet.models.transactions.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Simon on 24.08.18.
 */

public class AccountController {

    private BAccountDAO accDAO;
    private BTransactionDAO transDAO;

    private ArrayList<BAccount> accounts;
    private ArrayList<BTransaction> transactions;

    private BTransactionRecyclerAdaper transactionRecyclerAdaper;

    public AccountController(AppDatabase db){
        this.accDAO = db.bAccoutDAO();
        this.transDAO = db.bTransactionDAO();

//        this.transDAO.insertAll(new BTransaction(500, "Money money", "food"));
//        this.transDAO.insertAll(new BTransaction(750, "Beeer", "food"));
        this.loadAccounts();
        this.loadTransactions();

    }

    private void loadAccounts(){
        this.accounts = new ArrayList(this.accDAO.getAll());
        if (accounts.size() == 0){
            // TODO remove if no longer needed
            // add new dummy account
            BAccount a = new BAccount("Girokonto", 10000);
            this.accDAO.insertAll(a);
        }
    }

    public void initRecyclerView(RecyclerView view) {

        this.transactionRecyclerAdaper = new BTransactionRecyclerAdaper(this.transactions);

//        List<String> data = new ArrayList<>();
//        List<BTransaction> transactions = ;
//        for (Transaction trans:  this.transactions){
//            data.add(trans.getDesc());
//        }
//        for (int i = 1; i <= this.accCtrl.getTransactionCounter(); i++) {
//            data.add(i + "");
//        }
        view.setAdapter(this.transactionRecyclerAdaper);
//        this.transactionRecyclerAdaper.setItems(data);
    }

    private void loadTransactions(){
        this.transactions = new ArrayList(this.transDAO.getAll());
        Log.v("AccountCtrl", this.transactions.size() + " transactions");
    }

    public void book(Account acc, BTransaction trans) {
        this.transDAO.insertAll(trans);
        this.transactions.add(0, trans);
        this.transactionRecyclerAdaper.notifyDataSetChanged();
//        acc.setBalance(trans.getValue());
    }

    public void undoTransaction(Account acc, Transaction trans) {
        acc.setBalance(trans.getValue() * (-1));
    }

    public int getTransactionCounter(){ return this.transactions.size();}

    public List<BTransaction> getTransactions(){ return this.transactions;}
}
