package com.example.simon.wallet.models.account;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Simon on 22.08.18.
 */

public abstract class Account {

    public static int COUNTER = 0;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "description")
    private String desc;

    @ColumnInfo(name = "balance")
    private int balance;

    @Ignore
    public Account(String desc, int balance) {
        Account.COUNTER++;
//        this.id = Account.COUNTER;
        this.desc = desc;
        this.balance = balance;
    }

    public Account(int id, String desc, int balance) {
        Account.COUNTER++;
        this.id = id;
        this.desc = desc;
        this.balance = balance;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance += balance;
    }

    public void setId(int id) {
        this.id = id;
    }
}
