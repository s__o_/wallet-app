package com.example.simon.wallet;

import android.annotation.SuppressLint;
import android.app.Dialog;
//import android.app.DialogFragment;
import android.app.Fragment;
//import android.app.FragmentTransaction;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simon.wallet.DAO.AppDatabase;
import com.example.simon.wallet.adapter.BTransactionRecyclerAdaper;
import com.example.simon.wallet.adapter.RecyclerViewAdapter;
import com.example.simon.wallet.controller.AccountController;
import com.example.simon.wallet.models.account.Account;
import com.example.simon.wallet.models.account.BAccount;
import com.example.simon.wallet.models.transactions.BTransaction;
import com.example.simon.wallet.models.transactions.Transaction;
import com.example.simon.wallet.view.BottomNavigationViewHelper;
import com.example.simon.wallet.view.FullScreenDialog;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FullScreenDialog.AddTransactionListener {

    public final static int EXPENSE = 1;
    public final static int INCOME = 2;
    public final static int TRANSFER = 3;

    private SpeedDialView mSpeedDialView;
    private ViewPager viewPager;
    private BottomNavigationView navigation;
    private List<View> viewList;

    private AccountController accCtrl;

    private RoomDatabase.Builder<AppDatabase> db = Room.databaseBuilder(this,
            AppDatabase.class,"budgetDB").allowMainThreadQueries();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accCtrl = new AccountController(this.db.build());

        setContentView(R.layout.activity_bottom_navigation);
//

        navigation = findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
//        initBottomNavigation(navigation);
        initView();
        initSpeedDial();
        initBottomSheet();


    }

    private void initView() {
        View view1 = getLayoutInflater().inflate(R.layout.dashboard_view, null);
        View view2 = getLayoutInflater().inflate(R.layout.budget_view, null);
        View view3 = getLayoutInflater().inflate(R.layout.chart_view, null);
        View view4 = getLayoutInflater().inflate(R.layout.dashboard_view, null);

        viewList = new ArrayList<>();
        viewList.add(view1);
        viewList.add(view2);
        viewList.add(view3);
        viewList.add(view4);

        TextView balance = view1.findViewById(R.id.balance_value);

        balance.setText(NumberFormat.getInstance().format(23467.12) + "€");

        viewPager = findViewById(R.id.view_pager_bottom_navigation);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(pageChangeListener);

        navigation = findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (position == 0) {
//                evaluate = (Integer) evaluator.evaluate(positionOffset, getResources().getColor(R.color.app_blue), getResources().getColor(R.color.app_green));
            } else if (position == 1) {
//                evaluate = (Integer) evaluator.evaluate(positionOffset, getResources().getColor(R.color.app_green), getResources().getColor(R.color.app_yellow));
            } else if (position == 2) {
//                evaluate = (Integer) evaluator.evaluate(positionOffset, getResources().getColor(R.color.app_yellow), getResources().getColor(R.color.app_red));
            } else {
//                evaluate = getResources().getColor(R.color.app_red);
            }
//            ((View) viewPager.getParent()).setBackgroundColor(evaluate);
        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    navigation.setSelectedItemId(R.id.menu_action_dashboard);
                    break;
                case 1:
                    navigation.setSelectedItemId(R.id.menu_action_budget);
                    break;
                case 2:
                    navigation.setSelectedItemId(R.id.menu_action_charts);
                    break;
                case 3:
                    navigation.setSelectedItemId(R.id.menu_action_settings);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private PagerAdapter pagerAdapter = new PagerAdapter() {
        @Override
        public int getCount() {
            return viewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(viewList.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(viewList.get(position));
            return viewList.get(position);
        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_action_dashboard:
                    viewPager.setCurrentItem(0);
                    Log.v("MainActivity", "Dashboard");
//                        viewPager.setCurrentItem(0);
                    return true;
                case R.id.menu_action_budget:
                    viewPager.setCurrentItem(1);
                    Log.v("MainActivity", "Budgets");
//                        viewPager.setCurrentItem(1);
                    return true;
                case R.id.menu_action_charts:
                    viewPager.setCurrentItem(2);
                    Log.v("MainActivity", "Charts");
//                        viewPager.setCurrentItem(2);
                    return true;
                case R.id.menu_action_settings:
                    viewPager.setCurrentItem(3);
                    Log.v("MainActivity", "Settings");
//                        viewPager.setCurrentItem(3);
                    return true;
                default:
                    Log.v("MainActivity", "DEFAULT");
            }
            return false;
        }
    };

    private void initBottomSheet() {
//        final LayoutInflater factory = getLayoutInflater();

//        View dashboard = factory.inflate(R.layout.dashboard_view, null);
        View bottomSheet = viewList.get(0).findViewById(R.id.bottom_sheet);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        bottomSheetBehavior.setPeekHeight(520);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    SpeedDialView fba = findViewById(R.id.speedDial);
                    fba.setVisibility(View.INVISIBLE);
                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    SpeedDialView fba = findViewById(R.id.speedDial);
                    fba.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        RecyclerView recyclerView = viewList.get(0).findViewById(R.id.recycler_view_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        this.accCtrl.initRecyclerView(recyclerView);
    }

//    private void initRecyclerView(RecyclerView view) {
//
//        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this);
//        this.transactionRecyclerAdaper = new BTransactionRecyclerAdaper(this);
//
//        List<String> data = new ArrayList<>();
//        List<BTransaction> transactions = this.accCtrl.getTransactions();
//        for (Transaction trans:  transactions){
//            data.add(trans.getDesc());
//        }
//        for (int i = 1; i <= this.accCtrl.getTransactionCounter(); i++) {
//            data.add(i + "");
//        }
//
//        view.setAdapter(this.transactionRecyclerAdaper  );
//        adapter.setItems(data);
//    }


    private void initSpeedDial() {
        mSpeedDialView = viewList.get(0).findViewById(R.id.speedDial);
        mSpeedDialView.setMainFabClosedBackgroundColor(ResourcesCompat.getColor(getResources(),
                R.color.colorPrimary,
                getTheme()));
        mSpeedDialView.setMainFabOpenedBackgroundColor(ResourcesCompat.getColor(getResources(),
                R.color.colorPrimaryDark,
                getTheme()));
        mSpeedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_expense, R.mipmap.ic_expense)
                        .setLabel("Expense")
                        .setLabelBackgroundColor(Color.WHITE)
                        .create()
        );

        mSpeedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_income, R.mipmap.ic_income)
                        .setLabel("Income")
                        .setLabelBackgroundColor(Color.WHITE)
                        .create()
        );

        mSpeedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_transaction, R.mipmap.ic_transaction)
                        .setFabBackgroundColor(ResourcesCompat.getColor(getResources(),
                                R.color.colorPrimary,
                                getTheme()))
                        .setFabImageTintColor(ResourcesCompat.getColor(getResources(), android.R
                                .color.white, getTheme()))
                        .setLabel("Transaction")
                        .setLabelBackgroundColor(Color.WHITE)
                        .create()
        );


        mSpeedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {
            @Override
            public boolean onActionSelected(SpeedDialActionItem speedDialActionItem) {
                switch (speedDialActionItem.getId()) {
                    case R.id.fab_expense:
                        showFullscreenDialog(EXPENSE);
                        Log.v("MainActivity", "add new expense");
                        return false; // true to keep the Speed Dial open
                    case R.id.fab_income:
                        showFullscreenDialog(INCOME);
                        Log.v("MainActivity", "add new income");
                        return false; // true to keep the Speed Dial open
                    case R.id.fab_transaction:
                        showFullscreenDialog(TRANSFER);
                        Log.v("MainActivity", "add new transaction");
                        return false; // true to keep the Speed Dial open
                    default:
                        return false;
                }
            }
        });
    }


    private void showFullscreenDialog(int dialogType) {
        int style = 0;
        int layout = 0;

        switch (dialogType) {
            case EXPENSE:
                style = R.style.DialogFullscreenExpense;
                layout = R.layout.dialog_fullscreen_expense;
                break;
            case INCOME:
                style = R.style.DialogFullscreenIncome;
                layout = R.layout.dialog_fullscreen_expense;
                break;
            case TRANSFER:
                style = R.style.DialogFullscreenTransfer;
                layout = R.layout.dialog_fullscreen_expense;
                break;
        }
        /*
        final Dialog fullscreenDialog = new Dialog(this, style);
        fullscreenDialog.setContentView(layout);
        */

//        FragmentTransaction ft = getFragmentManager().beginTransaction();
        FragmentManager fm = getSupportFragmentManager();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");

//        ft.addToBackStack(null);
        final FullScreenDialog fullscreenDialog = FullScreenDialog.newInstance(style, layout, dialogType);


        fullscreenDialog.show(fm, "dialog");

    }

    @Override
    public void onBackPressed() {
        //Closes menu if its opened.
        if (mSpeedDialView.isOpen()) {
            mSpeedDialView.close();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void addTransaction(Transaction trans) {
        this.accCtrl.book(new BAccount("Test", 0), (BTransaction) trans);
        Log.v("MainActivity", trans.getDesc());
    }
}
