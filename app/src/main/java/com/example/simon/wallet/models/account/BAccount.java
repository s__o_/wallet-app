package com.example.simon.wallet.models.account;

import android.arch.persistence.room.Entity;

/**
 * Created by Simon on 18.11.18.
 */

@Entity
public class BAccount extends Account {

    public BAccount(String desc, int balance) {
        super(desc, balance);
    }
}
