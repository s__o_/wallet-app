# Wallet

![logo](/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png)

This app helps you to track your personal expenses and income. Use categories to organize all transactions and create different budgets, like monthly clothing, food or your personal saving goal, for  different aspects of your life.

### Features

- [x] adding and storing transactions
- [ ] custom budgets
- [ ] account overview (dashboard)
- [ ] financial analytics

## Impressions

### Main Activity

![main activity](/pictures/main_panel.png)

### Adding new Transactions

![income transaction](/pictures/income_panel.png)

![new expense transaction](/pictures/expense_panel.png)


## Requirements
