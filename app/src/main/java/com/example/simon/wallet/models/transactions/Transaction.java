package com.example.simon.wallet.models.transactions;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Simon on 22.08.18.
 */

public abstract class Transaction {

    public static int COUNTER = 0;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "value")
    private int value;

    @ColumnInfo(name = "description")
    private String desc;

    @ColumnInfo(name = "category")
    private String category;

    @ColumnInfo(name = "date")
    private String date;


    @Ignore
    public Transaction(int value, String desc, String category, String date) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Transaction.COUNTER++;
//        this.date = dateFormat.format(new Date()).toString();
//        this.id = Transaction.COUNTER;
        this.value = value;
        this.desc = desc;
        this.date = date;
        this.category = category;
    }

    public Transaction(int id, int value, String desc, String category, String date){
        Transaction.COUNTER ++;
        this.id = id;
        this.value = value;
        this.desc = desc;
        this.category = category;
        this.date = date;
    }

    public String getValueText(){
        DecimalFormat df = new DecimalFormat("0.00");
        double a = ((this.value)/ 100.00);

        return NumberFormat.getInstance().format(a) + "€";
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public String getDate() {
        return date;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }
}
